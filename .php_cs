<?php

$finder = PhpCsFixer\Finder::create()
    ->files()
    ->name('*.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true)
    ->in(__DIR__)
    ->exclude('campaigns')
    ->exclude('ci')
    ->exclude('db')
    ->exclude('locales')
    ->exclude('static')
    ->exclude('templates')
    ->exclude('tests')
    ->exclude('tex')
    ->exclude('tmp')
    ->exclude('vendor')
    ->exclude('_support')
;

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2' => true
    ])
    ->setUsingCache(true)
    ->setFinder($finder)
;
