<?php

namespace LQDN\Command;

class UserUpdateByAdminCommand
{
    private $id;
    private $username;
    private $email;
    private $comment;
    private $cumul;
    private $total;

    /**
     * __construct
     *
     * @param int $id
     * @param string $username
     * @param string $email
     * @param string $comment
     * @param string $cumul
     * @param string $total
     */
    public function __construct($id, $username, $email, $comment, $total, $cumul)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->comment = $comment;
        $this->total = $total;
        $this->cumul = $cumul;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getCumul()
    {
        return $this->cumul;
    }
}
