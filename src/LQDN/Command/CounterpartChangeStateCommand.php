<?php

namespace LQDN\Command;

class CounterpartChangeStateCommand
{
    private $counterpartId;
    private $state;

    public function __construct($counterpartId, $state)
    {
        $this->counterpartId = $counterpartId;
        $this->state = $state;
    }

    public function getCounterpartId()
    {
        return $this->counterpartId;
    }

    public function getState()
    {
        return $this->state;
    }
}
