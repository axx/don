<?php

namespace LQDN\Handler;

use Doctrine\DBAL\Connection;
use LQDN\Command\AdminChangePasswordCommand;
use LQDN\Command\AdminCreateCommand;
use LQDN\Command\AdminDeleteCommand;

class AdminHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Create a new admin.
     *
     * @param AdminCreateCommand $command
     */
    public function handleAdminCreateCommand(AdminCreateCommand $command)
    {
        $this->connection->executeUpdate('INSERT INTO admins (user_id, password) VALUES (:username, :password)', [
            'username' => $command->getUsername(),
            'password' => hash('sha256', $command->getPassword()),
        ]);
    }

    /**
     * Delete an admin.
     *
     * @param AdminDeleteCommand $command
     */
    public function handleAdminDeleteCommand(AdminDeleteCommand $command)
    {
        $this->connection->executeUpdate('DELETE FROM admins WHERE admins.id = :id', ['id' => $command->getId()]);
    }

    /**
     * Update the password of an admin.
     *
     * @param AdminChangePasswordCommand $command
     */
    public function handleAdminChangePasswordCommand(AdminChangePasswordCommand $command)
    {
        $this->connection->executeUpdate('UPDATE admins SET password = :password WHERE id = :id', [
            'password' => hash('sha256', $command->getPassword()),
            'id' => $command->getId(),
        ]);
    }
}
