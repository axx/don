<?php

namespace LQDN\Handler;

use Doctrine\DBAL\Connection;
use LQDN\Command\AddressCreateCommand;
use LQDN\Command\AddressDeleteCommand;
use LQDN\Command\AddressUpdateCommand;
use LQDN\Exception\AddressAlreadyExistsException;
use LQDN\Exception\AddressUsedException;

class AddressHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Delete an address.
     *
     * @param AddressDeleteCommand $command
     */
    public function handleAddressDeleteCommand(AddressDeleteCommand $command)
    {
        // Before deleting, we need to recover the adresse_id and remove it from
        // dons and contreparties table.
        $user_id = $command->getUserId();
        $addressId = $command->getAddressId();
        // Let's check if the address is used
        if ($this->addressUsed($addressId) == true) {
            // The address is used somehow
            throw new AddressUsedException();
        };
        $this->connection->executeUpdate("DELETE FROM adresses WHERE id = :id", ['id' => $addressId]);
    }

    /**
     * Create an address.
     *
     * @param AddressCreateCommand $command
     */
    public function handleAddressCreateCommand(AddressCreateCommand $command)
    {
        $userId = $command->getUserId();

        $query =<<<EOF
REPLACE INTO adresses(user_id, nom, adresse, adresse2, codepostal, ville, etat, pays)
VALUES (:user_id, :name, :address, :address2, :postal_code, :city, :state, :country)
EOF;

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('user_id', $command->getUserId());
        $stmt->bindValue('name', $command->getName());
        $stmt->bindValue('address', $command->getAddress());
        $stmt->bindValue('address2', $command->getAddress2());
        $stmt->bindValue('postal_code', $command->getPostalCode());
        $stmt->bindValue('city', $command->getCity());
        $stmt->bindValue('state', $command->getState());
        $stmt->bindValue('country', $command->getCountry());
        $stmt->execute();
    }

    /**
     * Check if an address already exists.
     *
     * @param int $userId
     *
     * @return bool
     */
    private function addressExists($userId)
    {
        return (bool) $this->connection->fetchColumn(
            "SELECT 1 FROM adresses WHERE user_id = :user_id",
            [
                'user_id' => $userId,
            ],
            0
        );
    }

    /**
     * Check if an adress is used.
     *
     * @param int $addressId
     *
     * @return bool
     */
    private function addressUsed($addressId)
    {
        $count = (int) $this->connection->fetchColumn(
            "SELECT count(1) FROM contreparties, dons WHERE contreparties.adresse_id = :id AND contreparties.status = 1;",
            [
                'id' => $addressId,
            ],
            0
        );
        return ($count > 0);
    }
}
