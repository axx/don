<?php

namespace LQDN\Finder;

use Doctrine\DBAL\Connection;

class DonationFinder
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * findByUserId
     *
     * @param int $userId
     *
     * @return []
     */
    public function findByUserId($userId)
    {
        $userId = (int) $userId;
        return $this->connection->executeQuery("SELECT * FROM dons WHERE user_id=:userId", ["userId" => $userId])->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * findById
     *
     * @param int $donationId
     *
     * @return []
     */
    public function findById($did)
    {
        return $this->connection->executeQuery("SELECT * FROM dons WHERE id=:did", ["did" => $did])->fetch(\PDO::FETCH_ASSOC);
    }
    /**
     * Return dons for admins.
     *
     * @param string $text
     * @param int $sum
     * @param mixed $public
     * @param status $status
     * @param int $limit
     *
     * @return array
     */
    public function adminSearch($text, $sum, $public, $status, $date1, $date2 = "2999-12-31 23:59:59", $limit = 50, $page = 1)
    {
        $query = <<<EOQ
SELECT d.id as id,
    d.datec AS datec,
    d.somme AS somme,
    d.pdf AS pdf,
    d.status AS status,
    d.user_id AS user_id,
    u.email AS email,
    u.pseudo AS pseudo,
    a.nom AS nom,
    a.adresse AS adresse,
    a.adresse2 AS adresse2,
    a.codepostal AS codepostal,
    a.ville AS ville,
    a.pays AS pays
FROM dons d
LEFT JOIN users u ON u.id = d.user_id
LEFT JOIN adresses a ON d.adresse_id = a.id
WHERE 1=1
EOQ;

        $params = [];

        if ('' !== $text) {
            $query .= " AND (d.id like :text OR u.email like :text OR a.nom like :text OR a.adresse like :text OR a.adresse2 like :text)";
            $params['text'] = "%$text%";
        }

        if ('' !== $sum) {
            $query .= ' AND d.somme = :sum';
            $params['sum'] = $sum;
        }

        if ('' !== $public) {
            $query .= ' AND d.public = :public';
            $params['public'] = $public;
        }
        if ('' !== $status) {
            $query .= ' AND d.status IN (:status) ';
            $params['status'] = $status;
        }

        if ('' !== $date1) {
            $query .= ' AND d.datec BETWEEN (:date1) AND (:date2) ';
            $params['date1'] = $date1;
            $params['date2'] = $date2;
        }
        $first = ($page - 1) * $limit;
        $last = ($page) * $limit;
        $query .= " ORDER BY datec DESC LIMIT $first, $last";

        return $this->connection->fetchAll($query, $params);
    }
}
