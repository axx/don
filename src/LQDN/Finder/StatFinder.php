<?php

namespace LQDN\Finder;

use Doctrine\DBAL\Connection;

class StatFinder
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Return donations par hour since $startingHour.
     *
     * @param \DateTime $startingHour
     *
     * @return array
     */
    public function donationsPerHour(\DateTime $startingHour = null)
    {
        if (null == $startingHour) {
            $startingHour = (new \DateTime())->modify('-24 hours');
        }

        $query = <<<EOQ
select
  concat(date(datec),' ',hour(datec),'h') 'heure',
  concat(count(*),' dons') 'nombre de dons',
  concat(sum(somme), ' EUR') 'total',
  concat(floor(avg(somme)),' EUR/don') 'moyenne'
from dons
where status in (1,4,101)
  and datec > :date
group by (concat(date(datec),' ',hour(datec)))
order by datec DESC
EOQ;

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('date', $startingHour, 'datetime');

        return $stmt->fetchAll();
    }

    /**
     * Return 30 latest donations
     *
     * @return []
     */
    public function latestDonations()
    {
        $query = <<<EOQ
select datec 'date', concat(somme,' EUR') 'montant', lang 'langue'
from dons
where status in (1,4,101)
order by datec DESC
limit 30
EOQ;

        return $this->connection->fetchAll($query);
    }

    /**
     * Return the average donation amount per day since $ŝtartingDay.
     *
     * @param \DateTime $startingDay
     *
     * @return []
     */
    public function averageDonationPerDay(\DateTime $startingDay)
    {
        $query = <<<EOQ
select avg(nombre) as moyenne
from (
    select count(*) as nombre
    from dons
    where datec > :date
      and status in (1,4,101)
    group by year(datec), month(datec), day(datec)
) as t
EOQ;

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('date', $startingDay, 'datetime');

        return $stmt->fetchAll();
    }

    /**
     * Return the average donation amount from $startingDay.
     *
     * @param \DateTime $startingDay
     *
     * @return []
     */
    public function averageDonationAmount(\DateTime $startingDay)
    {
        $stmt = $this->connection->prepare('select avg(somme) as moyenne from dons where datec > :date and status in (1,4,101)');
        $stmt->bindValue('date', $startingDay, 'datetime');

        return $stmt->fetchAll();
    }

    /**
     * Return the repartition of donation's amount per day during last 30 days.
     *
     * @return []
     */
    public function donationsRepartitionByDay()
    {
        $query = <<<EOQ
select year(datec) as annee, month(datec) as mois, day(datec) as jour, count(*) as nombre, sum(somme) as montant
from dons
where datec > '2013-01-01'
  and status in (1,4,101)
group by year(datec), month(datec), day(datec)
order by datec desc
limit 30
EOQ;

        return $this->connection->fetchAll($query);
    }

    /**
     * Return the donation repartition by amount.
     *
     * @param \DateTime $startingDay
     *
     * @return []
     */
    public function donationsRepartitionByAmount(\DateTime $startingDay)
    {
        $query = <<<EOQ
select count(*) nombre, concat(somme,' EUR') 'montant', sum(somme) total
from dons
where status in (1,4,101)
  and datec > :date
group by (somme)
order by somme DESC, nombre desc
limit 30
EOQ;

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('date', $startingDay, 'datetime');

        return $stmt->fetchAll();
    }
}
