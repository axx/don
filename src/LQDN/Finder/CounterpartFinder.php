<?php

namespace LQDN\Finder;

use Doctrine\DBAL\Connection;

class CounterpartFinder
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Return a counterpart by its id
     *
     * @param mixed $id
     *
     * @return array
     */
    public function findById($id)
    {
        $id = (int) $id;
        $stmt = $this->connection->executeQuery('SELECT * FROM contreparties WHERE id = :id', [ 'id' => $id ]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Return all counterparts of a given user.
     *
     * @param mixed $userId
     *
     * @return array
     */
    public function findByUserId($userId)
    {
        $userId = (int) $userId;
        $counterparts = [];
        $stmt = $this->connection->executeQuery('SELECT * FROM contreparties WHERE user_id = :user_id', ['user_id' => $userId]);
        while ($counterpart = $stmt->fetch()) {
            $counterpart['pdf_id'] = '';
            $counterpart['pdf_nom'] = '';
            $counterpart['pdf_url'] = '';

            // Of course, piplomes are messy
            // @TODO: This part is probably not working.
            if ('piplome' === $counterpart['quoi']) {
                $query = <<<EOQ
SELECT d.id as id, a.nom as pseudo, d.pdf as pdf
FROM dons d
JOIN adresses a ON a.id = d.adresse_id
JOIN contreparties c ON c.id = d.taille
WHERE d.taille = :size
EOQ;

                $pdf = $this->connection->fetchAssoc($query, [
                    'size' => $counterpart['taille']
                ]);
                if (false !== $pdf) {
                    $counterpart['pdf_id'] = $pdf['id'];
                    $counterpart['pdf_nom'] = $pdf['pseudo'];
                    $counterpart['pdf_url'] = $pdf['pdf'];
                }
            }

            $counterparts[$counterpart['id']] = $counterpart;
        }

        return $counterparts;
    }

    /**
     * return the id of the next item to be inserted
     *
     * @return int
     */
    public function getNextInsertedId()
    {
        return (int) $this->connection->fetchColumn(
            "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'contreparties' AND table_schema = DATABASE()"
        );
    }

    /**
     * Return all counterparts in a specific status
     *
     * @param mixed $status
     *
     * @return array
     */
    public function findByStatus($status)
    {
        $status = (int) $status;
        $counterparts = [];
        $stmt = $this->connection->executeQuery('SELECT * FROM contreparties WHERE status = :status', ['status' => $status]);
        while ($counterpart = $stmt->fetch()) {
            $counterpart['pdf_id'] = '';
            $counterpart['pdf_nom'] = '';
            $counterpart['pdf_url'] = '';

            // Of course, piplomes are messy
            // @TODO: This part is probably not working.
            if ('piplome' === $counterpart['quoi']) {
                $query = <<<EOQ
SELECT d.id as id, a.nom as pseudo, d.pdf as pdf
FROM dons d
JOIN adresses a ON a.id = d.adresse_id
JOIN contreparties c ON c.id = d.taille
WHERE d.taille = :size
EOQ;

                $pdf = $this->connection->fetchAssoc($query, [
                    'size' => $counterpart['taille']
                ]);
                if (false !== $pdf) {
                    $counterpart['pdf_id'] = $pdf['id'];
                    $counterpart['pdf_nom'] = $pdf['pseudo'];
                    $counterpart['pdf_url'] = $pdf['pdf'];
                }
            }

            $counterparts[$counterpart['id']] = $counterpart;
        }

        return $counterparts;
    }

    /** Return all counterparts by quoi
     *
     * @param mixed $quoi
     *
     * @return array
     */
    public function findByQuoi($quoi)
    {
        $quoi = (string) $quoi;
        $counterparts = [];
        $stmt = $this->connection->executeQuery('SELECT * FROM contreparties WHERE quoi = :quoi', ['quoi' => $quoi]);
        while ($counterpart = $stmt->fetch()) {
            $counterpart['pdf_id'] = '';
            $counterpart['pdf_nom'] = '';
            $counterpart['pdf_url'] = '';

            // Of course, piplomes are messy
            // @TODO: This part is probably not working.
            if ('piplome' === $counterpart['quoi']) {
                $query = <<<EOQ
SELECT d.id as id, a.nom as pseudo, d.pdf as pdf
FROM dons d
JOIN adresses a ON a.id = d.adresse_id
JOIN contreparties c ON c.id = d.taille
WHERE d.taille = :size
EOQ;

                $pdf = $this->connection->fetchAssoc($query, [
                    'size' => $counterpart['taille']
                ]);
                if (false !== $pdf) {
                    $counterpart['pdf_id'] = $pdf['id'];
                    $counterpart['pdf_nom'] = $pdf['pseudo'];
                    $counterpart['pdf_url'] = $pdf['pdf'];
                }
            }

            $counterparts[$counterpart['id']] = $counterpart;
        }

        return $counterparts;
    }
}
