<?php

namespace LQDN;

use Doctrine\DBAL\DriverManager;
use LQDN\Finder;
use LQDN\Handler;
use Pimple\Container as BaseContainer;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

class Container extends BaseContainer
{
    public function __construct()
    {
        parent::__construct([
            'db' => function ($d) {
                $connection = DriverManager::getConnection([
                    'driver' => 'pdo_mysql',
                    'dbname' => SQL_DATABASE,
                    'host' => SQL_HOST,
                    'port' => SQL_PORT,
                    'user' => SQL_USER,
                    'password' => SQL_PASSWORD,
                ]);
                $connection->query('SET NAMES UTF8');

                return $connection;
            },

            'session' => function ($c) {
                $pdo = new \PDO(SQL_DSN, SQL_USER, SQL_PASSWORD, [
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                ]);

                $storage = new NativeSessionStorage(
                    [
                        'name' => 'LQDNSESSID'
                    ],
                    new PdoSessionHandler($pdo, [
                        'db_table' => 'sessions',
                        'db_id_col' => 'session_id',
                        'db_data_col' => 'data',
                        'db_lifetime_col' => 'lifetime',
                        'db_time_col' => 'stamp',
                        'lock_mode' => PdoSessionHandler::LOCK_NONE,
                    ])
                );

                return new Session($storage);
            },

            'command_handler' => function ($c) {
                return new CommandHandler([
                    new Handler\AddressHandler($c['db']),
                    new Handler\DonationHandler($c['db']),
                    new Handler\CounterpartHandler($c['db']),
                    new Handler\AdminHandler($c['db']),
                    new Handler\UserHandler($c['db']),
                ]);
            },

            // Finders
            'address_finder' => function ($c) {
                return new Finder\AddressFinder($c['db']);
            },
            'admin_finder' => function ($c) {
                return new Finder\AdminFinder($c['db']);
            },
            'counterpart_finder' => function ($c) {
                return new Finder\CounterpartFinder($c['db']);
            },
            'donation_finder' => function ($c) {
                return new Finder\DonationFinder($c['db']);
            },
            'stat_finder' => function ($c) {
                return new Finder\StatFinder($c['db']);
            },
            'user_finder' => function ($c) {
                return new Finder\UserFinder($c['db']);
            },
        ]);
    }
}
