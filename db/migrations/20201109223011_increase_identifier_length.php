<?php

use Phinx\Migration\AbstractMigration;

class IncreaseIdentifierLength extends AbstractMigration
{
    public function up()
    {
        $this->adapter->execute("ALTER TABLE dons modify COLUMN identifier VARCHAR(255) NULL;");
    }
}
