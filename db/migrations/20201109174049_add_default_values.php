<?php

use Phinx\Migration\AbstractMigration;

class AddDefaultValues extends AbstractMigration
{
    public function up()
    {
        $this->adapter->execute("ALTER TABLE contreparties ALTER datec SET DEFAULT '0000-00-00 00:00:00';");
        $this->adapter->execute("ALTER TABLE contreparties ALTER commentaire SET DEFAULT '';");

        $this->adapter->execute("ALTER TABLE dons ALTER datec SET DEFAULT '0000-00-00 00:00:00';");
        $this->adapter->execute("ALTER TABLE dons ALTER cadeau SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER abo SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER taille SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER public SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER pdf SET DEFAULT \"\";");
        $this->adapter->execute("ALTER TABLE dons ALTER decimale SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER datee SET DEFAULT '0000-00-00 00:00:00';");
        $this->adapter->execute("ALTER TABLE dons ALTER pi_x SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER pi_y SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE dons ALTER hash SET DEFAULT \"\";");
        $this->adapter->execute("ALTER TABLE dons ALTER taille_h SET DEFAULT '0';");

        $this->adapter->execute("ALTER TABLE users ALTER total SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE users ALTER cumul SET DEFAULT '0';");
        $this->adapter->execute("ALTER TABLE users ALTER pseudo SET DEFAULT \"\";");
        $this->adapter->execute("ALTER TABLE users ALTER commentaire SET DEFAULT \"\";");
    }
}
