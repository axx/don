<?php

use Phinx\Seed\AbstractSeed;

class IdentifiersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'expiration' => '2016-06-10 12:34',
                'user_id' => 1,
		'identifier' => 'id1',
            )
        );

        $this->table('identifiers')->insert($data)->save();
    }
}
