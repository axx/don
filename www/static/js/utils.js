function show(id) {
    document.getElementById(id).className = 'on';
}

function hide(id) {
    document.getElementById(id).className = 'off';
}

function show_hide(id, ids) {
    show(id);
    for (k in ids) {
        hide(ids[k]);
    }
}

