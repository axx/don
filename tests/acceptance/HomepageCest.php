<?php
class HomepageCest
{
    public function frontpageWorks(AcceptanceTester $I)
    {
        $I->wantTo('Ensure that homepage works.');
        $I->amOnPage('/');
        $I->see('Soutenez La Quadrature du Net !');
    }

    public function faqpageWorks(AcceptanceTester $I)
    {
        $I->wantTo('Ensure that FAQ works.');
        $I->amOnPage('/faq');
        $I->see('Foire aux questions');
    }
}
