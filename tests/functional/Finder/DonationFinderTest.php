<?php

namespace LQDN\Tests\Functional\Finder;

use LQDN\Tests\Functional\FunctionalTest;

class DonationFinderTest extends FunctionalTest
{
    public function testFindByUserId()
    {
        $donations = $this->container['donation_finder']->findByUserId(1);
        $this->assertCount(1, $donations);

        // Check the first donation
        $expectedDonation = [
            'id' => '1',
            'user_id' => '1',
            'status' => '1',
            'datec' => '2016-06-10 12:34:00',
            'somme' => '1000',
            'lang' => 'fr_FR',
            'cadeau' => '1',
            'abo' => '1',
            'taille' => '2',
            'public' => '0',
            'pdf' => 'pdf',
            'decimale' => '2',
            'datee' => '2016-06-10 12:34:00',
            'mailsent' => '1',
            'color' => 'blue',
            'pi_x' => '3',
            'pi_y' => '3',
            'hash' => 'hash',
            'taille_h' => '8',
            'fdnn_user' => '1234567890',
            'color_2' => 'red',
            'cumul' => '1000',
            'adresse_id' => '1',
            'identifier' => 'id1',
        ];
        $this->assertEquals($expectedDonation, $donations[0]);
    }

    public function testAdminSearchDonations()
    {
        $this->assertCount(1, $this->container['donation_finder']->adminSearch($text='alice@example.org', $sum='', $public='', $status='', $date1=''));
        $this->assertCount(2, $this->container['donation_finder']->adminSearch($text='', $sum='1000', $public='', $status='', $date1=''));
        $this->assertCount(2, $this->container['donation_finder']->adminSearch($text='', $sum='', $public='0', $status='', $date1=''));
        $this->assertCount(2, $this->container['donation_finder']->adminSearch($text='', $sum='', $public='', $status='1', $date1=''));
        $this->assertCount(2, $this->container['donation_finder']->adminSearch($text='', $sum='', $public='', $status='1', $date1=''));
        $this->assertCount(1, $this->container['donation_finder']->adminSearch($text='', $sum='', $public='', $status='', $date1='2016-06-11 00:00:00'));

    }
}
