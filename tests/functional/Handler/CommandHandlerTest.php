<?php

namespace LQDN\Tests\Functionnal\Handler;

use LQDN\CommandHandler;
use LQDN\Tests\Functional\FunctionalTest;

class CommandHandlerTest extends Functionaltest
{
    public function testCommandNotAnObjectException()
    {
        $this->setExpectedException('LQDN\Exception\CommandNotAnObjectException');
        $this->container['command_handler']->handle("Not a command object");
    }

    public function testCommandNotHandledException()
    {
        $this->setExpectedException('LQDN\Exception\CommandNotHandledException');
        $this->container['command_handler']->handle((object)[]);
    }
}

?>
