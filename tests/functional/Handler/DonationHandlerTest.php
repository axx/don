<?php

namespace LQDN\Tests\Functional\Handler;

use LQDN\Command\DonationInvalidateCommand;
use LQDN\Command\DonationResetPdfCommand;
use LQDN\Command\DonationValidateCommand;
use LQDN\Command\DonationCreateCommand;
use LQDN\Command\DonationIncStatusCommand;
use LQDN\Tests\Functional\FunctionalTest;

class DonationHandlerTest extends FunctionalTest
{
    public function testDonationValidation()
    {
        $this->assertEquals(1, $this->getDonation(1)['status']);
        $this->container['command_handler']->handle(new DonationInvalidateCommand(1));
        $this->assertEquals(0, $this->getDonation(1)['status']);
        $this->container['command_handler']->handle(new DonationValidateCommand(1));
        $this->assertEquals(1, $this->getDonation(1)['status']);
    }

    public function testDonationResetPdf()
    {
        $this->assertEquals('pdf', $this->getDonation(1)['pdf']);
        $this->container['command_handler']->handle(new DonationResetPdfCommand(1));
        $this->assertEquals('', $this->getDonation(1)['pdf']);
    }


    private function testDonationByUser()
    {
        $this->assertEquals(2, count($this->getDonationByUser(2)));
        $this->assertEquals(2, $this->getDonationByUser(2)[0]['user_id']);
    }

    public function testDonationCreateCommand()
    {
        $this->container['command_handler']->handle(new DonationCreateCommand(1, 1, date("Y-m-d H:i:s"), 50, 0, 0));

    }

    public function testDonationIncStatusCommand()
    {
        // Let's get the previous status
        $don = $this->container['donation_finder']->findById(1);
        $prev_status = $don['status'];

        // Update
        $this->container['command_handler']->handle(new DonationIncStatusCommand($don['id']));
        $don = $this->container['donation_finder']->findById(1);
        $this->assertEquals($prev_status + 1, $don['status']);

        // Remise en état
        $this->container['db']->executeUpdate("UPDATE dons SET status = status - 1 WHERE id = :id",
        [ 'id' => $don['id']]);
    }
    /**
     * Retrieve a donation
     *
     * @return []
     */
    private function getDonation($id)
    {
        return $this->container['db']->fetchAssoc("SELECT * FROM dons WHERE id = $id");
    }

    /**
     * Retrieve donations by userid
     *
     * @return []
     */
    private function getDonationByUser($uid)
    {
        return $this->container['db']->fetchAll("SELECT * FROM dons WHERE user_id = $uid");
    }
}
