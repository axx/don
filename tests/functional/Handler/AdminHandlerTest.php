<?php

namespace LQDN\Tests\Functional\Handler;

use LQDN\Command\AdminChangePasswordCommand;
use LQDN\Command\AdminCreateCommand;
use LQDN\Command\AdminDeleteCommand;
use LQDN\Tests\Functional\FunctionalTest;

class AdminHandlerTest extends FunctionalTest
{
    public function testAdminCreate()
    {
        $this->container['command_handler']->handle(new AdminCreateCommand('foobar', 'password'));

        $admin = $this->getLatestAdmin();

        $this->assertSame('foobar', $admin['user_id']);
        $this->assertSame(hash('sha256', 'password'), $admin['password']);
    }

    public function testAdminDelete()
    {
        $this->assertEquals(1, $this->getAdmin(1)['id']);
        $this->container['command_handler']->handle(new AdminDeleteCommand(1));

        $this->assertFalse($this->getAdmin(1));
    }

    public function testAdminChangePassword()
    {
        $this->assertEquals(hash('sha256', 'password'), $this->getAdmin(1)['password']);
        $this->container['command_handler']->handle(new AdminChangePasswordCommand(1, 'newpassword'));
        $this->assertEquals(hash('sha256', 'newpassword'), $this->getAdmin(1)['password']);
    }

    /**
     * Retrieve a given admin
     *
     * @param int $id
     *
     * @return []
     */
    private function getAdmin($id)
    {
        return $this->container['db']->fetchAssoc("SELECT * FROM admins WHERE id = $id");
    }

    /**
     * Retrieve the latest created admin
     *
     * @return []
     */
    private function getLatestAdmin()
    {
        return $this->container['db']->fetchAssoc("SELECT * FROM admins ORDER BY id desc LIMIT 1");
    }
}
