<?php

namespace LQDN\Tests\Functional\Handler;

use LQDN\Command\AddressCreateCommand;
use LQDN\Command\AddressDeleteCommand;
use LQDN\Command\AddressUpdateCommand;
use LQDN\Handler\AddressHandler;
use LQDN\Tests\Functional\FunctionalTest;

class AddressHandlerTest extends FunctionalTest
{
    public function testAddressUsedDelete()
    {
        $this->assertTrue($this->addressExists(1));

        $this->setExpectedException('LQDN\Exception\AddressUsedException');
        $this->container['command_handler']->handle(new AddressDeleteCommand(1, 1));

        $this->container['command_handler']->handle(new AddressDeleteCommand(2, 1));

        $this->assertFalse($this->addressExists(2));
    }

    public function testAdressDelete()
    {
        $this->assertTrue($this->addressExists(2));

        $this->container['command_handler']->handle(new AddressDeleteCommand(2, 1));

        $this->assertFalse($this->addressExists(2));
    }

    public function testAddressCreate()
    {
        $this->assertTrue($this->addressExists(1));

        $this->container['command_handler']->handle(new AddressCreateCommand(1, 1, '6 rue Ménars', '', 75002, 'Paris', 'France', 'IDF'));
    }

    public function testAddressCreateWithSpecialChars()
    {
        $this->assertTrue($this->addressExists(1));

        $this->container['command_handler']->handle(new AddressCreateCommand(1, 'New address', '6 rue Ménars', '', 22000, 'Saint Brieuc', 'France', 'Côtes d\'Armor'));

        $expectedAddress = [
            'nom' => 'New address',
            'adresse' => '6 rue Ménars',
            'adresse2' => '',
            'codepostal' => '22000',
            'ville' => 'Saint Brieuc',
            'etat' => 'Côtes d\'Armor',
            'pays' => 'France',
            'user_id' => '1',
        ];

        $latestAddress = $this->getLatestAddress();
        unset($latestAddress['id']);

        $this->assertSame($expectedAddress, $latestAddress);
    }

    /**
     * Check if an address exists in DB.
     *
     * @param int $id
     *
     * @return bool
     */
    private function addressExists($id)
    {
        return (bool) $this->container['db']->fetchColumn("SELECT 1 FROM adresses WHERE id = $id");
    }

    /**
     * Retrieve latest address.
     *
     * @return []
     */
    private function getLatestAddress()
    {
        return $this->container['db']->fetchAssoc("SELECT * FROM adresses ORDER BY id DESC LIMIT 1");
    }
}
