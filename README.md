# LQDN soutien

This is a php application based on Fat-Free-Framework and a MySQL database that handles donations and presents offered to donors to La Quadrature du Net

It's used as our primary donation platform, and uses our bank's payment system (cheaper) to get one-time or recurring payments recorded into the donation platform.

## Installation (quick)

In order to install this project, run `make doctor` to check that everything is fine.

Then you can run `make install` to install the project. Follow instructions if needed.
To install the DB, you need to run `make reset-db` (it will remove your local DB if it exists).

Once everything if fine, you can run a simple PHP server with `make server-start`

You have those accounts to test:

* alice@example.org / password
* bob@example.org / password

To access to the admin (`/admin`) use `admin`/`password`

## Contributing

Before submitting a PR, makes sure tests are OK: `make test`

# Detailed dependencies, installation ...

## Dependencies

This software depends on a few software :

* **composer** (to install php dependencies) see https://getcomposer.org/
* a **mysql client** (to inject the database schema)
* a **mysql server** (no need for it to be local)
* **pdftk** to generate pi-plomes (pdf with pi's decimals offered to our donors)
* **texlive-latex-base** for the `pdflatex` binary for the piplomes (only required in production or for people debugging the piplomes code)

and for developers you may also need :

* **xgettext** to manage translations, use:
  * `make messages.pot` to update PO files from source code.
  * `make translations` to compile MO files from PO files when translations are ready.

This software uses:

* **PHP7.3**
* **phinx** to inject database schema and initial data / accounts into MySQL see https://phinx.org/ 
* **php-cs-fixer** to check php code for errors and fix coding standard issues, see https://github.com/FriendsOfPHP/PHP-CS-Fixer
* **doctrine/dbal** as ORM see https://github.com/doctrine/dbal 
* **f3 framework** as main view / controller framework. see https://fatfreeframework.com/

Look at `app/` folder for most Controllers, and `app/routes.ini` for the application routes (the URLs)

# Production deployment (not finished)

* `app/env` must contains the proper values : database connection, bank visa payment codes, and ENV=production at the bottom.
* point your Nginx or Apache with PHP5.6 to the `www/` folder, and either allow Rewrite Rules (a2enmod rewrite) or point every URL not being a file to index.php (see below for an nginx sample)
* use AND FORCE https usage, it's 2018, people ;) 

Nginx configuration sample:

```
location ~ \.php$ {
    include fastcgi_params;
    fastcgi_read_timeout 1d; #avoid timeout when importing bank data
    fastcgi_pass unix:/run/php7.3.sock;
}
location / {
   # try to serve file directly, fallback to index.php
   try_files $uri /index.php$is_args$args;
}
```			


